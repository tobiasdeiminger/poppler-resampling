import argparse
import os
import itertools
import tempfile
from subprocess import check_output, CalledProcessError, Popen, PIPE
import matplotlib.pyplot as plt
import numpy

IM7_CONVERT_BIN = "/usr/local/bin/convert"
IM7_COMPARE_BIN = "/usr/local/bin/compare"
RENDER_TO_PAGE_BIN = "./build/scaletest"

REF_DPI = 300

def save_image_in_pdf(infile, outfile, dpi):
    check_output([IM7_CONVERT_BIN, '-density', str(dpi), infile, outfile])


def render_to_page(infile, outfile, frontend, dpi):
    print("{} {} {} {} {}".format(RENDER_TO_PAGE_BIN, infile, outfile, frontend, str(dpi)))
    out = check_output([RENDER_TO_PAGE_BIN, infile, outfile, frontend, str(dpi)])
    print(out)


def get_image_size(infile):
    p = Popen([IM7_CONVERT_BIN, infile, '-format', '%w %h', 'info:'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    out,err = p.communicate()
    w, h = out.split()
    return (float(w), float(h))


def resize_image(infile, outfile, x, y, resample_filter):
    check_output([IM7_CONVERT_BIN, '-resize', '{}x{}'.format(x, y), '-filter', resample_filter, infile, outfile])

def stitch_images(infiles, outfile):
    check_output([IM7_CONVERT_BIN, '-bordercolor', '#000000', '-border', '1'] + infiles + ['+append', outfile])

def compare(infile, outfile, metric):
    p = Popen([IM7_COMPARE_BIN, '-metric', metric, infile, outfile, 'null:'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    out,err = p.communicate()
    return float(err)

def make_text_image(outdir, outpdf, text_to_render):
    ref_image = os.path.join(tempdir, '{}.png'.format(outpdf))
    ref_pdf = os.path.join(tempdir, outpdf)
    try:
        out = check_output([IM7_CONVERT_BIN, '-background', '#ffffff', '-fill', '#000000', '-font', '/usr/share/fonts/opentype/freefont/FreeSerif.otf', '-pointsize', '400', '-gravity', 'NorthWest', 'caption:{}'.format(text_to_render), '-flatten', ref_image])
    except CalledProcessError as e:
        print(e.cmd, e.output)
    save_image_in_pdf(ref_image, ref_pdf, REF_DPI)

def plot(x, graphs, metric):
    title='{} comparison of downsampling by Splash vs. Cairo'.format(metric)
    y_axis='averrage {}'.format(metric)
    colors = itertools.cycle(['green', 'blue', 'red'])
    for graph in graphs:
        plt.plot(x, graphs[graph], color=next(colors), label=graph, marker='o')
    plt.title(title)
    plt.xlabel('resampling factor')
    plt.ylabel(y_axis)
    plt.legend()
    plt.show()


def test_scale_down_and_measure(ref_pdf, scales, poppler_frontend, metric, tempdir):
    """Compare downsampling with algo_under_test to downsampling using Lanczos (best possible)"""
    ssim_arr = []
    golden_image = os.path.join(tempdir, '{}_golden.png'.format(poppler_frontend))
    render_to_page(ref_pdf, golden_image, poppler_frontend, 1. * 300)
    for scale in scales:
        downsampled_image = os.path.join(tempdir, '{}_{}.png'.format(poppler_frontend, str(scale)))
        resized_image = os.path.join(tempdir, '{}_golden_resized.png'.format(poppler_frontend))
        render_to_page(ref_pdf, downsampled_image, poppler_frontend, scale * 300)
        resized_x, resized_y = get_image_size(downsampled_image)
        resize_image(golden_image, resized_image, resized_x, resized_y, 'Lanczos')
        ssim = compare(downsampled_image, resized_image, metric)
        ssim_arr.append(ssim)
    return ssim_arr

def test_text_scaling(scales, poppler_frontend, tempdir, pdf_file):
    ref_pdf = os.path.join(tempdir, pdf_file)
    generated = []
    for scale in scales:
        downsampled_image = os.path.join(tempdir, 'text_{}_{}.png'.format(poppler_frontend, str(scale)))
        render_to_page(ref_pdf, downsampled_image, poppler_frontend, scale * 300)
        generated.append(downsampled_image)
    return generated

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("image")
    args = parser.parse_args()
    tempdir = tempfile.mkdtemp()
    ref_image = os.path.abspath(args.image)
    ref_pdf = os.path.join(tempdir, 'test.pdf')
    REF_X,REF_Y = get_image_size(ref_image)
    print("Running test in {}. Original image size {} x {}.".format(tempdir, REF_X, REF_Y))

    save_image_in_pdf(ref_image, ref_pdf, REF_DPI)

    delta = 0.005
    scales = numpy.arange(1. - delta, 0. + delta, -delta)
    ssim_qt = test_scale_down_and_measure(ref_pdf, scales, 'qt', 'SSIM', tempdir)
    psnr_qt = test_scale_down_and_measure(ref_pdf, scales, 'qt', 'PSNR', tempdir)
    ssim_glib = test_scale_down_and_measure(ref_pdf, scales, 'glib', 'SSIM', tempdir)
    psnr_glib = test_scale_down_and_measure(ref_pdf, scales, 'glib', 'PSNR', tempdir)
    plot(scales, {'Splash': ssim_qt, 'Cairo': ssim_glib}, 'SSIM')
    plot(scales, {'Splash': psnr_qt, 'Cairo': psnr_glib}, 'PSNR')

    make_text_image(tempdir, 'text.pdf', 'y3')
    text_images = test_text_scaling([0.7], 'qt', tempdir, 'text.pdf') + test_text_scaling([0.7], 'glib', tempdir, 'text.pdf')
    stitch_images(text_images, os.path.join(tempdir, 'text_images_side_by_side.png'))
