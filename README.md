# poppler-resampling

Poppler is a free and open source library to render PDF documents. It's used by Okular and Evince readers. One of many pipeline steps is to scale embedded raster images up or down. There have been bug reports claiming Okular "looks worse" compared to Evince at certain zoomlevels:
- [Okular #408222](https://bugs.kde.org/show_bug.cgi?id=408222)
- [Poppler #950](https://gitlab.freedesktop.org/poppler/poppler/-/issues/950) (was [Okular #424817](https://bugs.kde.org/show_bug.cgi?id=424817))

I can't recognize a difference amongst the bad and good example images in the bug reports, my bad CV taste just won't let me.

But there's an obvious difference, namely the downsampling algorithm when either [poppler/qt5](https://gitlab.freedesktop.org/poppler/poppler/-/blob/master/qt5/src/poppler-qt5.h) or [poppler/glib](https://gitlab.freedesktop.org/poppler/poppler/-/blob/master/glib/poppler.h) frontend is used. I'm intereseted in the topic, so here's some stuff to learn about the used algorithm and try to find metrics to compare them.

## Behind the scenes

Poppler comes with multiple render engines. The used resampling algorithm depends (amongst others) on the used render engine.
- [Splash](https://gitlab.freedesktop.org/poppler/poppler/tree/poppler-0.77.0/splash), a builtin engine inherited from xpdf. Seen in action when using Okular. Natively supports color spaces CMYK and DeviceN8 (CMYK with spot colors), which is important for offset printing and not offered by Cairo.
- [Cairo](https://gitlab.freedesktop.org/cairo/cairo), a popular open source 2D rendering library, notably used in GTK and Libre Office (before 7, they changed to skia recently). Widely adopted, hardware specific optimizations, but internally only supports RGB.
- Arthur, the painting system of the Qt library.

Cpp frontend uses Splash, Qt5 frontend uses Splash or Artuhr, Glib frontend uses Cairo.

### Scaling with Splash

For downsampling, or vast upsampling (> 4), Splash implements a [Bresenham](https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm) algorithm.

Bresenham was originally invented to interpolate lines between two points when plotting on a raster. It was designed to be fast, which is achieved by only using cheap integer addition, subtraction and bit shifting. Bresenham can be used for resampling as well, as articles by Thiadmer Riemersma explain:
- [Image Scaling With Bresenham](https://www.drdobbs.com/image-scaling-with-bresenham/184405045)
- [Quick colour averaging](https://www.compuphase.com/graphic/scale3.htm)

Popplers Bresenham routines have been inherited from xpdf. I can't tell yet how much they're related to above articles. Bresenham is used for souce/dest iteration, and the destination pixel is calculated by [averaging](https://gitlab.freedesktop.org/poppler/poppler/blob/poppler-0.77.0/splash/Splash.cc#L4317) the pixels values in a small sliding source rectangle.
- [`Splash::scaleImageYdXd`](https://gitlab.freedesktop.org/poppler/poppler/blob/poppler-0.77.0/splash/Splash.cc#L4220) scales down in y and x direction
- [`Splash::scaleImageYdXu`](https://gitlab.freedesktop.org/poppler/poppler/blob/poppler-0.77.0/splash/Splash.cc#L4463) scales down in y, and up in x direction
- [`Splash::scaleImageYuXd`](https://gitlab.freedesktop.org/poppler/poppler/blob/poppler-0.77.0/splash/Splash.cc#L4613) scales up in y, and down in x direction
- [`Splash::scaleImageYuXu`](https://gitlab.freedesktop.org/poppler/poppler/blob/poppler-0.77.0/splash/Splash.cc#L4775) scales up in y and x direction

If interpolation is explicitly wanted by the PDF image XObject, or if image is scaled up only moderatly (1..4), [`Splash::scaleImageYuXuBilinear`](https://gitlab.freedesktop.org/poppler/poppler/blob/poppler-0.77.0/splash/Splash.cc#L4956) does bilinear interpolation, added in 2012 with [this commit](https://gitlab.freedesktop.org/poppler/poppler/commit/a97aead193a927b91a4e33d7b5d2ba7179e664df) by Adrian Johnson in response to [bug 22138](https://bugs.freedesktop.org/show_bug.cgi?id=22138).

If bug reports have a point, Splash scaling code is what most likely needs changes. [Splash Scaling](splash_scaling.md) is my take to gain deeper understanding and to arm up for potential merge requests.

### Scaling with Glib/Cairo

Glib frontend uses [Cairo](https://cairographics.org/), which uses [Pixman](http://www.pixman.org/). Pixman contains the actual scaling algorithm and implements them with hardware acceleration for various instruction sets.

#### How poppler uses Cairo

[`CairoOutputDev::getFilterForSurface`](https://gitlab.freedesktop.org/poppler/poppler/blob/poppler-0.77.0/poppler/CairoOutputDev.cc#L1958) tells cairo to mostly use `CAIRO_FILTER_GOOD` (actual implementation depends on Cairo version), or `CAIRO_FILTER_NEAREST` (nearest neighbor) in some corner cases.

#### How Cairo uses Pixman

For `CAIRO_FILTER_GOOD`, [Cairo uses](https://github.com/freedesktop/cairo/blob/7cb362d5861df09b990ea81bbbb66099cab3a2a5/src/cairo-image-source.c#L970) the newer "seperable convolution API" of Pixman, where the filter kerenl [is defined in Cairo](https://github.com/freedesktop/cairo/blob/7cb362d5861df09b990ea81bbbb66099cab3a2a5/src/cairo-image-source.c#L623), but image iteration and transformation happens in Pixman. Delegating to Pixman most notably involves theses steps:
- Cairo calculates the parameters for the seperable convolution filter, despite the existence of a pixman helper function with same purpose. Formerly [they used the builtin function](https://gitlab.freedesktop.org/cairo/cairo/commit/fb57ea13e04d82866cbc8e86c83261148bb3e231), why [the change](https://lists.cairographics.org/archives/cairo/2014-July/025388.html)?
- Cairo sets a dest-to-source (!) transformation matrix that defines the scaling parameter sx and sy [pixman_image_set_transform](https://github.com/freedesktop/cairo/blob/7cb362d5861df09b990ea81bbbb66099cab3a2a5/src/cairo-image-source.c#L939)
- Cairo tells pixman which filter to use for the source image [by means of pixman_image_set_filter](https://github.com/freedesktop/cairo/blob/7cb362d5861df09b990ea81bbbb66099cab3a2a5/src/cairo-image-source.c#L1021)
- Cairo calls [pixman_image_composite32](https://github.com/freedesktop/cairo/blob/7cb362d5861df09b990ea81bbbb66099cab3a2a5/src/cairo-image-compositor.c#L538) to perform the scale and filter operations and store it in a destination image

#### How Pixman performs the operation

In [Pixman](https://gitlab.freedesktop.org/pixman/pixman), the work starts [here](https://gitlab.freedesktop.org/pixman/pixman/blob/pixman-0.38.4/pixman/pixman.c#L700), where `func` is a function pointer to a `pixman_composite_func_t`.
```
typedef void (*pixman_composite_func_t) (pixman_implementation_t *imp,
					 pixman_composite_info_t *info);
```
and where `imp` holds pointers to `pixman_combine_32_func_t` function. Check `FAST_NEAREST_SCANLINE` and `FAST_NEAREST_MAINLOOP_INT` for details.

For some implementations, follow [`static const pixman_fast_path_t c_fast_paths[]`](https://gitlab.freedesktop.org/pixman/pixman/blob/pixman-0.38.4/pixman/pixman-fast-path.c#L1815).

#### Pixman filter API

Pixman has now two APIs for image resampling. The traditional API takes one of
```
typedef enum
{
    PIXMAN_FILTER_FAST,
    PIXMAN_FILTER_GOOD,
    PIXMAN_FILTER_BEST,
    PIXMAN_FILTER_NEAREST,
    PIXMAN_FILTER_BILINEAR,
    PIXMAN_FILTER_CONVOLUTION,
    //...
}
```

The newer one, implemented by Søren Sandmann Pedersen in 2012 with [this](https://gitlab.freedesktop.org/pixman/pixman/commit/6fd480b17c8398c217e4c11e826c82dbb8288006) and [this](https://gitlab.freedesktop.org/pixman/pixman/commit/6915f3e24f4169260a8ad6ab7ff3087388dbe5db) commits, specifically aims to improve image downsampling quality.

> The motivation for this new filter is to improve image downsampling
> quality. Currently, the best pixman can do is the regular convolution
> filter which is limited to coarsely sampled convolution kernels.

It allows to calculate a convolution kernel up front, where one can select from
```
  - IMPULSE:            Dirac delta function, ie., point sampling
  - BOX:                Box filter
  - LINEAR:             Linear filter, aka. "Tent" filter
  - CUBIC:              Cubic filter, currently Mitchell-Netravali
  - GAUSSIAN:           Gaussian function, sigma=1, support=3*sigma
  - LANCZOS2:           Two-lobed Lanczos filter
  - LANCZOS3:           Three-lobed Lanczos filter
  - LANCZOS3_STRETCHED  Three-lobed Lanczos filter, stretched by 4/3.0.
                        This is the "Nice" filter from Dirty Pixels by
                        Jim Blinn.
```

That said, pixman is mostly undocumented. What I write here is the result of greping mailing lists, looking at pixman test suite and code, and checking client code like Cairo or X.
To gain more confidence, I'm writing a heavily documented [pixman hello world](hello_pixman/hello.c) along.

#### Pixman hardware acceleration

Pixman uses so called "fast paths" to leverage specialized processor instruction sets for speed, mainly [SIMD](https://en.wikipedia.org/wiki/SIMD) instructions.

Profiling pixman in `./scaletest test.pdf test.pdf.png glib 280` with [sprof](https://man7.org/linux/man-pages/man1/sprof.1.html) activated on my Intel Core i7 CPU reveals we're spending all the time in a hardware accelerated function in `fast_composite_scaled_bilinear_sse2_8888_n_8888_none_OVER`. Note that `sprof` seems broken, I had to recomplie glibc with a patch as suggested in the [Inconsistency detected by ld.so bug entry](https://sourceware.org/bugzilla/show_bug.cgi?id=22380#c8) to get it working.
```
$ LD_LIBRARY_PATH=./amd64-libc/:./amd64-libc/dlfcn/ amd64-libc/elf/ld.so amd64-libc/elf/sprof /home/tobias/daten/build/pixman-0.38.4/build/pixman/.libs/libpixman-1.so.0.38.4 /tmp/sprof/libpixman-1.so.0.profile -p
Flat profile:

Each sample counts as 0,01 seconds.
  %   cumulative   self              self     total
 time   seconds   seconds    calls  us/call  us/call  name
100,00   4859,16  4859,16        0     0,00           fast_composite_scaled_bilinear_sse2_8888_n_8888_none_OVER
  0,00   4859,17     0,01        0     0,00           fast_composite_scaled_bilinear_sse2_8888_8888_normal_SRC
  0,00   4859,17     0,00       10     0,00           bits_image_fetch_bilinear_affine_pad_r5g6b5
  0,00   4859,17     0,00        3     0,00           bits_image_fetch_nearest_affine_pad_r5g6b5
  0,00   4859,17     0,00        3     0,00           bits_image_fetch_separable_convolution_affine_normal_a8
  0,00   4859,17     0,00        3     0,00           store_scanline_g1
  0,00   4859,17     0,00        2     0,00           bits_image_fetch_separable_convolution_affine_none_r5g6b5
```

You won't find such a function in pixman source code. It's defined by a platform independent [preprocessor template](https://gitlab.freedesktop.org/pixman/pixman/-/blob/master/pixman/pixman-inlines.h#L612).
```
#define FAST_NEAREST_MAINLOOP(scale_func_name, scanline_func, src_type_t, dst_type_t,		\
			      repeat_mode)							\
	FAST_NEAREST_MAINLOOP_NOMASK(_ ## scale_func_name, scanline_func, src_type_t,		\
			      dst_type_t, repeat_mode)
```

and then a [platform specific invocation](https://gitlab.freedesktop.org/pixman/pixman/-/blob/master/pixman/pixman-sse2.c#L5423) of that macro.
```
FAST_NEAREST_MAINLOOP (sse2_8888_8888_none_OVER,
                       scaled_nearest_scanline_sse2_8888_8888_OVER,
                       uint32_t, uint32_t, NONE)
```

The resulting function uses [scaled_nearest_scanline_sse2_8888_8888_OVER](https://gitlab.freedesktop.org/pixman/pixman/-/blob/master/pixman/pixman-sse2.c#L5318) to iterate source and destination buffers. It assumes that a source pixel consists of a `uint32_t` (one byte for each channel), and a destination pixel consists of a `uint32_t` again.

Best entry point to learn more about pixman hardware optimizations is probably to follow authors patch set disscussions on their mailing list (including links to benchmarks), like:
- [pixman: fast path for nearest neighbour scaled compositing operations.](https://lists.cairographics.org/archives/cairo/2009-May/017211.html)
- [SSSE3 iterator and fast path selection issues](https://lists.freedesktop.org/archives/pixman/2013-August/002867.html)
- [sse2: faster bilinear scaling (pack 4 pixels to write with MOVDQA)](https://lists.freedesktop.org/archives/pixman/2013-September/002883.html)
- [SSSE3 iterator for bilinear scaling](https://lists.freedesktop.org/archives/pixman/2013-September/002897.html)
- [New fast paths and Raspberry Pi 1 benchmarking](https://lists.freedesktop.org/archives/pixman/2015-August/003855.html)
- [Add support for aarch64 neon optimization](https://lists.freedesktop.org/archives/pixman/2016-March/004487.html)

## Quality assesment of scaling algorithm

The basic workflow is:
1. Start with some original image ([zone plate](https://en.wikipedia.org/wiki/Zone_plate)). Embedd it into a `test.pdf` file.
1. Use both poppler Qt and glib frontend libraries to scale the PDF to different target sizes.
1. Also scale the the original image down using Lanczos3 as high quality filter, so that we have something of same size to compare with.
1. Compare scale result of poppler to scale result of Lanczos using different metrics ([SSIM](https://en.wikipedia.org/wiki/Structural_similarity), [PSNR](https://en.wikipedia.org/wiki/Peak_signal-to-noise_ratio)).
1. Record the metrics as a graph for comparision.

Compile the `render_to_image` program
```
$ sudo apt install build-essential libpoppler-dev libpoppler-glib-dev libpoppler-qt5-dev
$ mkdir build/ && cd build/
$ cmake ../render_to_image/
$ cmake --build ./
$ chmod +x scaletest
```

Install recent Image Magick 7
```
$ apt build-dep imagemagick
$ wget https://www.imagemagick.org/download/ImageMagick.tar.gz
$ tar xvzf ImageMagick.tar.gz
$ cd ImageMagick-7.0.10-24/ && ./configure && make
$ sudo make install
```

Run
```
$ sudo apt install python-matplotlib
$ python run.py images/z128.gif
```

## Results

### Visual inspection of output
Some rendered glyphs from a generated text image after scaling to 70%. From left to right: Splash using Bresenham, Cairo using bilinear filtering.
![](results/text_images_side_by_side.png)

### Measure SSIM (reference is Lanczos3)
Splash is blue, Cairo is green. The higher the value, the better. Goes from zoom level 0% left to zoom level 100% (i.e. original size, no downscaling) on the right side.

![](results/ssim.png)

### Measure PSNR (reference is Lanczos3)
Splash is blue, Cairo is green. The higher the value, the better. Goes from zoom level 0% left to zoom level 100% (i.e. original size, no downscaling) on the right side.
![](results/psnr.png)

### Measure CPU time
I've put a `std::chrono::steady_clock::now()` at the beginning and the end of `Gfx::doImage` to compare algorithm in terms of CPU time.
Test environment was
```
model name      : Intel(R) Core(TM) i7 CPU         860  @ 2.80GHz
cpu MHz         : 1880.459
cache size      : 8192 KB
cpu cores       : 4
```

![](results/cpu_time_us.png)

## Interpretation

Disclaimer: I don't have much experience in this field. Test method and interpretation may be totally incorrect.

I belive results show the following:
- Edges of glyphs look jagged when downscaled with Splah, smoother when downscaled with Cairo.
- SSIM and PSNR metrics suggest that Cairo produces better quality than Splash at most zoomlevels.
- We clearly see the speed change when Cairo [switches](https://lists.cairographics.org/archives/cairo/2014-June/025282.html) from box filter (100% .. 75%) to a bilinear filter (< 75%). When using a box filter, Cairo is considerably faster than Splash. That's interesting, because Splashs Bresenham also does kind of box filtering if I got it right. After switching to bilinear filtering, Cairo becomes slower than Splash.

## Approaches for improvement

### Use Adrians linear interpolation filter for everything, not only for vast upscaling

TODO: How much does quality improve? What's the performance penalty?

### Add yet another DIY scaling implementation in Splash

TODO: If so, which filter to implement?

A simplistic variant is probably doable in a few hundred LOC.
But would be much more work to get up to par with e.g. pixmans hardware optimization.

### Use libpixman for scaling in Splash

Pro:
- Tried it in a quick and dirty private patch, seems not too hard and produces good quality.
- Minimalisitc dependency, available for virtually every target.
- Good optimizations.
- Well tested due to wide spread use since about 2005 (as part of Cairo, X11, Wayland's Weston)

Con:
- Adding a new dependency to CMake target `poppler` (i.e. libpoppler.so) may not be loved by everyone.
- Pixman doesn't support CMYK. For pure scaling that might be solved easily: Use ARGB to get the amount of channels right. At least Splash does nothing special when scaling CMYK, it calculates a destination CMYK pixel just the same as it does for a RGB pixel.
- Pixman doesn't support Device8. We can't fake it with pixman, but will have to either leave a remaining splash implementation to handle the Device8, or to convert color space somehow.
- Splash can read a PDF stream row by row. With pixman it [might be necessary](https://www.mail-archive.com/pixman@lists.freedesktop.org/msg02263.html) to fetch the whole image into a buffer instead of reading it line by line. It's possible to use custom accessors instead, but I don't know if they work with seperable convolution filters.

### Use QPixmap for scaling in Qt5 frontend

Pro:
- Splash remains untouched, no additional dependency as Qt is there anyway
- Good optimizations, I'd assume.
- Tested and prooven.

Con:
- Non-Qt clients (e.g. pdf2ppm) won't benefit
- Only course control of used filter (`Qt::FastTransformation`, `Qt::SmoothTransformation`)

### Use ImageMagick for scaling in Splash

There [has been a patch](https://bugs.freedesktop.org/show_bug.cgi?id=22138#c13) which was not acceted.
Didn't check technical details yet. Seems the controversy was not that technical anyway.

Pro:
- Natively supports CMYK
Con:
- Heavy dependency added to `libpoppler.so`
- Don't think it would support DeviceN8, but am not really sure

## Computer vision basics
- [convolution kernel](https://en.wikipedia.org/wiki/Kernel_(image_processing))
- [reconstruction filter kernels](https://en.wikipedia.org/wiki/Reconstruction_filter#Image_processing)
- [bilinear interpolation](https://en.wikipedia.org/wiki/Bilinear_interpolation)
- [bicubic interpolation](https://en.wikipedia.org/wiki/Bicubic_interpolation)
- [nearest neighbor interpolation](https://en.wikipedia.org/wiki/Nearest-neighbor_interpolation)
- [lanczos resampling](https://en.wikipedia.org/wiki/Lanczos_resampling)
