#include <glib/poppler.h>
#include <gobject/gobject.h>
#include <poppler-qt5.h>
#include <QDebug>
#include <QImage>
#include <QString>
#include <cairo.h>
#include <iostream>
#include <memory>

/* Controls the used algo in my patched poppler buidl */
extern int scaleAlgo;

void renderToImageWithGlib(const char* filename, int page, const char* outputPng, double dpi) {
    GError *error;
    gchar *uri = g_filename_to_uri (filename, NULL, &error);    
    PopplerDocument *document = poppler_document_new_from_file (uri, NULL, &error);
    if (document == NULL) {
        printf("poppler fail: %s\n", error->message);
        return;
    }
    PopplerPage *pdfPage = poppler_document_get_page (document, page);
    double width, height;
    poppler_page_get_size(pdfPage, &width, &height);
    cairo_surface_t *surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, dpi * width / 72.0, dpi * height / 72.0);
    cairo_t *cr = cairo_create (surface);
    cairo_scale(cr, dpi / 72.0, dpi / 72.0);
    poppler_page_render(pdfPage, cr);
    cairo_destroy(cr);
    cairo_surface_write_to_png(surface, outputPng);
    cairo_surface_destroy (surface);
    g_object_unref(pdfPage);
    g_object_unref(document);
}

void renerToImageWithQt(const char* filename, int page, const char* outputPng, double dpi) {
    std::unique_ptr<Poppler::Document> document { Poppler::Document::load(filename) };
    /* Load document and get first page. */
    if (!document || document->isLocked()) {
      std::cout << "Can't open document";
    }
    std::unique_ptr<Poppler::Page> pdfPage { document->page(page) };
    if (pdfPage == 0) {
      std::cout << "Can't open page";
    }
    QImage pageImage;
    pageImage = pdfPage->renderToImage(dpi, dpi, -1, -1, -1, -1);
    pageImage.save(outputPng);
}

int main(int argc, char* argv[]) {
    char* inputPdf = argv[1];
    int page = atoi(argv[2]);
    char* outputPng = argv[3];
    char* popplerFrontend = argv[4];
    double dpi = atof(argv[5]);

    if (argc == 7) {
        scaleAlgo = atoi(argv[6]);
    }

    if (strcmp(popplerFrontend, "qt") == 0)
        renerToImageWithQt(inputPdf, page, outputPng, dpi);
    else if (strcmp(popplerFrontend, "glib") == 0)
        renderToImageWithGlib(inputPdf, page,  outputPng, dpi);
    else
        std::cout << "Unknown poppler frontend";

    return 0;
}
