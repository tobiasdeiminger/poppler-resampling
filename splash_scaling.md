# Splash scaling

Let's try to understand the ins and outs of
```
void Splash::scaleImageYdXd(SplashImageSource src, void *srcData, SplashColorMode srcMode, int nComps, bool srcAlpha,
                            int srcWidth, int srcHeight, int scaledWidth, int scaledHeight, SplashBitmap *dest)
```

## Inputs

### PDF images

Terms in the flollowing senteces will only make sense if one reads the PDF standard along.

At the earliest stage we have a sampled image in a PDF document as input. It's a PDF stream object with an `Image` dictionary (Type = XObject, Subtype = Image) followed by data that are the raw samples. The samples may optionally be encoded for compression by one of the PDF standard filters like LZWDecode, JBIG2Decode, DCTDecode, JPXDecode. The number of color components per sample is determined by the color space, e.g. DeviceGray means 1 color per sample, DeviceRGB means 3 colors per sample. Colors are stored interleaved, like `[r,g,b,r,g,b,r,g,b,...]` .

Transparency may be achieved by a soft mask, which is a second image XObject, linked to by the first image XObject.

There's yet another kind of mask, the explicit mask, to define pixels where the base image shall not be painted.

### Poppler processing of PDF images

Popplers internal representation of such a stream object is a `class Stream` instance, where `Stream::getDict` would provide the image dictionary, and `Stream::getStream` would provide the raw samples.

Such an object is painted by the PDF operator [`Do`](https://gitlab.freedesktop.org/poppler/poppler/-/blob/master/poppler/Gfx.cc#L150), which poppler [delegates](https://gitlab.freedesktop.org/poppler/poppler/-/blob/master/poppler/Gfx.cc#L4093) to [Gfx::doImage](https://gitlab.freedesktop.org/poppler/poppler/-/blob/master/poppler/Gfx.cc#L4132) where the image dictionary meta data is parsed.

Further processing and visible drawing depends:
- if image is a mask (entry `ImageMask` or `IM` is true), delgegate to `out->drawImageMask`
- if image has an explicit mask, `out->drawMaskedImage`
- if image has an soft mask (there's a `SMask` entry), call `out->drawSoftMaskedImage`
- if image has no mask, call `out->drawImage`

`out` is the output device. In the case of Splash, it's `class SplashOutputDev`.

Splash wraps the `Stream` into an [`ImageStream`](https://gitlab.freedesktop.org/poppler/poppler/-/blob/master/poppler/Stream.h#L424), which can [read one line of pixels](https://gitlab.freedesktop.org/poppler/poppler/-/blob/master/poppler/Stream.cc#L604) into a buffer.

### `Splash` members

To be done.

### Function argumnets

#### `SplashImageSource src`

`splash/SplashTypes.h`
```cpp
typedef unsigned char *SplashColorPtr;
```

`splash/Splash.h`
```cpp
// Retrieves the next line of pixels in an image.  Normally, fills in
// *<line> and returns true.  If the image stream is exhausted,
// returns false.
typedef bool (*SplashImageSource)(void *data, SplashColorPtr colorLine, unsigned char *alphaLine);
```

`SplashImageSource` is a function pointer type to fetch one row of pixels from the PDF stream into a buffer.

Actually there are two destination buffers:
- `colorLine` will receive one row of pixels according to the current `SplashColorMode`
- `alphaLine` will receive one row of alpha (opacity) information, if alpha is enalbed.

Implementations of such a function are `SplashOutputDev::alphaImageSrc` and `SplashOutputDev::imageSrc`.
`SplashOutputDev` decides which one to use before it calls into `drawImage`.

Multiple calls to `SplashImageSource` will automatically hop on to the next line.
This works because `data` can be casted into `SplashOutImageData` which tracks the current line in a `::y` member.

#### `void *srcData`

The actual source image buffer. It's simply used as `data` for the `SplashImageSource` function on each row fetch.
```cpp
(*src)(srcData, lineBuf, alphaLineBuf);
```
where `void*` will internally be casted to `SplashOutImageData*`.

#### `SplashColorMode srcMode`

```cpp
enum SplashColorMode {
  splashModeMono1,
  splashModeMono8,
  splashModeRGB8,
  splashModeBGR8,
  splashModeXBGR8,
  splashModeCMYK8,
  splashModeDeviceN8
};
```

This is *NOT* the color model of the to-be-scaled image. Instead, it is the internal mode of operation of the whole `SplashOutputDev` which remains constant for the lifetime of an output device.

Qt5 frontend almost always uses a XBGR8 output device, except when overprinting ([what's up with overprinting](https://lists.freedesktop.org/archives/poppler/2011-March/007249.html)?) when it uses DeviceN8.
```
QImage Page::renderToImage(/*...*/) {
    const SplashColorMode colorMode = overprintPreview ? splashModeDeviceN8 : splashModeXBGR8;
    // ...
    Qt5SplashOutputDev splash_output(colorMode, 4, false, ignorePaperColor, ignorePaperColor ? nullptr : bgColor, true, thinLineMode, overprintPreview);
    // ...
}
```

The PS Backend does it like this
```
PSOutputDev.cc:        splashOut = new SplashOutputDev(splashModeMono8, 1, false, paperColor, false);
PSOutputDev.cc:        splashOut = new SplashOutputDev(splashModeCMYK8, 1, false, paperColor, false);
PSOutputDev.cc:        splashOut = new SplashOutputDev(splashModeRGB8, 1, false, paperColor, false);
```

Color mode determines the memory layout of a pixmap buffer.

| Color Mode | Memory layout | Color space |
|------------|---------------|-------------|
| Mono1 | 1 bit per component, 8 pixels per byte, MSbit is on the left. | na |
| Mono8 | 1 byte per component, 1 byte per pixel. | na |
| RGB8  | 24 bytes per pixel, 3 components, 1 byte per component: RGBRGB... | additive (emit and mix light from a black monitor) |
| BGR8  | 24 bytes per pixel: 3 components, 1 byte per component: BGRBGR... | additive (emit and mix light from a black monitor) |
| XBGR8 | 32 bytes per pixel: 4 components (but 1 undefined), 1 byte per component: XBGRXBGR... High byte "wasted" with undefined data, which makes sense on 32bit hardware because operations are faster if pixels are aligned to integer boundaries. | additive (emit and mix light from a black monitor) |
| CMYK8 | 1 byte per component, 4 bytes per pixel: CMYKCMYK... Usefull when printing. | subtractive (notch away wave lengths from white paper) |
| DeviceN8 | 1 byte per component, 4 bytes + n bytes spot colors per pixel: CMYKSSSSCMYKSSSS... Specific format used in offset printing, see PMS (Pantone Matching System). | subtractive (notch away wave lengths from white paper) |

Note that Splash never interleaves pixels with alpha data. Alpha (if needed) is carried in a separate buffer.

#### `int nComps`

Number of color components per pixel, where alpha is not counted as color. For RGB it's `3`, for RGBA it's `3` too.

#### `bool srcAlpha`

True if the `SplashImageSource` data proivder function provides an additional `alphaLine` buffer.

#### `int srcWidth, int srcHeight`

Width and heigth of the original, unscaled source image in pixels.

#### `int scaledWidth, int scaledHeight`

Width and heigth of the resulting image after scaling.

## Outputs

### `SplashBitmap *dest`

The resulting image will be provided as `class SplashBitmap` instance. It holds raw image data

```cpp
private:
    // ...
    SplashColorPtr data; // pointer to row zero of the color data
    unsigned char *alpha; // pointer to row zero of the alpha data
```
along with meta data and accessors.

A `SplashBitmap` owns the image data until someone else takes ownership by calling `SplashBitmap::takeData`.
