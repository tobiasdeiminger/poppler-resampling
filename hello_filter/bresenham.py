import itertools
import numpy
import matplotlib.pyplot as plt

def plot(x, graphs, metric):
    title='{}'.format(metric)
    y_axis='{}'.format(metric)
    colors = itertools.cycle(['green', 'blue', 'red', 'cyan', 'yellow'])
    for graph in graphs:
        plt.plot(x, graphs[graph], color=next(colors), label=graph, marker='o')
    plt.title(title)
    plt.xlabel('resampling factor')
    plt.ylabel(y_axis)
    plt.legend()
    plt.show()

step_sizes = {}
def track_stepsizes(x, y, window_size):
    global step_sizes
    count = step_sizes.get(window_size, 0)
    step_sizes[window_size] = count + 1


def just_print(x, y, window_size):
    print("dst({}, {}): x_step = {}, y_step = {}".format(x, y, window_size[0], window_size[1]))


def iterate(src_w, src_h, dst_w, dst_h, analyze):
    # Bresenham parameters for y scale
    yp = src_h / dst_h
    yq = src_h % dst_h

    # Bresenham parameters for x scale
    xp = src_w / dst_w;
    xq = src_w % dst_w;

    yt = 0
    for y in range(dst_h):
        yt += yq
        if yt >= dst_h:
            yt -= dst_h
            y_step = yp + 1
        else:
            y_step = yp

        xt = 0
        d0 = 0
        d1 = 1
        for x in range(dst_w):
            xt += xq
            if xt >= dst_w:
                xt -= dst_w
                x_step = xp + 1
                d = d1
            else:
                x_step = xp
                d = d0
            analyze(x, y, (x_step, y_step))

def debug_varying_window():
    global step_sizes
    delta = 0.025
    count11 = []
    count12 = []
    count21 = []
    count22 = []
    # below 0.55 we get also bigger other than 2x2 sizes
    scales = numpy.arange(1., 0.1, -delta)
    for scale in scales:
        iterate(512, 512, int(512*scale), int(512*scale), track_stepsizes)
        count11.append(step_sizes.get((1, 1), 0))
        count12.append(step_sizes.get((1, 2), 0))
        count21.append(step_sizes.get((2, 1), 0))
        count22.append(step_sizes.get((2, 2), 0))
        step_sizes = {}
    plot(scales, {'1x1': count11, '1x2': count12, '2x1': count21, '2x2': count22}, 'watch window varying')

debug_varying_window()

