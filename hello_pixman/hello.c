/*
  Compile:
  $ gcc `pkg-config --cflags pixman-1` hello.c -Wl,`pkg-config --libs pixman-1`
*/

#include <pixman.h> // the Pixman API
#include <stdlib.h>
#include <stdio.h>

/* Utility function to write buffer to file. One can view it in GIMP as "Raw image from data". */
static void save_buffer(char* filename, uint32_t *buf, uint32_t bufsize) {
    FILE* f;
    f = fopen(filename, "wb");
    fwrite(buf, 1, bufsize, f);
    fclose(f);
}

/* Utility function to print colors er pixel on terminal. */
static void print_ascii(uint32_t *buf, int width, int height) {
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            const uint32_t pix = buf[y * height + x];
            const uint32_t a = (pix & 0xff000000) >> 24;
            const uint32_t r = (pix & 0x00ff0000) >> 16;
            const uint32_t g = (pix & 0x0000ff00) >> 8;
            const uint32_t b = (pix & 0x000000ff);
            printf("%02x,%02x,%02x,%02x", a, r, g, b);
            if (x < width-1)
                printf(" ");
        }
        printf("\n");
    }
        
}

/* Utility function to draw a test image into a buffer. */
static void draw_test_image_a8r8g8b8(uint32_t *buffer, int width, int height) {
    for (int y = 0; y < height; y++)
        for (int x = 0; x < width; x++) {
            uint32_t pixel;
            if (y == 0 || y == height - 1 || x == 0 || x == width - 1)
                pixel = (0xff << 24) | (255 << 16);
            else
                pixel = (0xff << 24) | (255 * (y % 2));
            buffer[y * height + x] = pixel;
        }
}

int main (int argc, char **argv) {
    const float scale = 0.5;
    const int width = 32, height = 32;
    const int dest_width = width * scale, dest_height = height * scale;
    uint32_t *source_buffer, *dest_buffer;

    /*
     * pixman_image_t is pixmans handle for an image.
     * A image may be a bitmap, in which case a byte buffer containg the raw pixels is associated (image->type == BITS).
     */
    pixman_image_t *src_img, *dest_img;

    /* A fixed point type of size 4, where 16 MSB are the integer part and 16 LSB are fractional part. */
    pixman_fixed_t s;

    /* Prepare source buffer and image. Pixman won't allocate the buffer, we have to do it manually. */
    source_buffer = calloc (1, width * height * sizeof(uint32_t));
    draw_test_image_a8r8g8b8(source_buffer, width, height);
    src_img = pixman_image_create_bits(
        PIXMAN_a8r8g8b8, // A pixel shall have 4 bytes = 32 bits: 8 bits for alpha, 8 bits for red, 8 bits for green, 8 bits for blue.
        width, height,   // width and height in pixels
        source_buffer,   // the underlying byte buffer
        width * 4        // Rowstride bytes, for cases where one needs padding after each row,
                         // to align rows on memory boundaries. We don't require padding here,
                         // so rowstride is simply size of row.
    );

    /* Prepare destination buffer and image. */
    dest_buffer = calloc (1, dest_width * dest_height * sizeof(uint32_t)); // allocate and 0-initilaize
    dest_img = pixman_image_create_bits(
        PIXMAN_a8r8g8b8,
        dest_width, dest_height,
        dest_buffer,             
        dest_width * 4           
    );

   /*
    * A pixman_transform_t is simply a 3x3 matrix.
    * We can represents projective (much more often affine)
    * 2d transformations, i.e. describe scale, translation and rotation.
    *
    * MIND THE UNEXPECTED: It is used as a destination-to-source matrix.
    * So we have to give numbers like "source is double (i.e. sx=2) the size of destination",
    * instead of "destination is half (sx=0.5) the size of source".
    */
    pixman_transform_t transform;

    /*
     * Calculate reverse (dest-to-source) scale, and convert it to fixed point type.
     * 65536 is 0x00010000. Multiplying something with 65536.0 basically means shift left by 16 bits,
     * but take factional part into account instead of filling right bits with 0.
     */
    s = (1 / scale) * 65536.0 + 0.5;
    printf("Scale source by %f (dest-to-source sx=sy=%u.%u)\n", scale, (s & 0xffff0000) >> 16, (s & 0x0000ffff));

    /*
     * Set matrix parameters so that they describe a scaling operation with
     * sx = s and sy = s. If s = 2, this means "source is double the size of destination",
     * which we more intuitively read as "destination is half the size of source".
     */
    pixman_transform_init_scale (&transform, s, s);

    /*
     * Set the transformation matrix.
     * It is used when we use that image as a source in a composite call later.
     */
    pixman_image_set_transform (src_img, &transform); // associate source image with the scaling transformation

    /*
     * The result of the scale transformation is stored in a 2D raster.
     * Input raster and output raster have different sizes,
     * so we need a filter for resampling.
     * It is used when we use that image as a source in a composite call later.
     */
    pixman_image_set_filter(
        src_img,
        PIXMAN_FILTER_BILINEAR,
        NULL,
        0
    );

    /*
     * Despite the name, pixman_image_composite() seems to be the
     * general purpose "now do it" API function in pixman.
     * (how about renaming it pixman_image_operate()?).
     *
     * It can not only blend two images, but it's also the way to go to
     * apply transformations and filters to the source image.
     * Whatever the operation is, the result will be stored into
     * the destination buffer.
     *
     * The first argument PIXMAN_OP_* refers to Porter Duff operators.
     * Pixman contributor Søren Sandmann Pedersen explains them well:
     * http://ssp.impulsetrain.com/porterduff.html
     *
     * Note that pixman always assumes premultiplied alpha when blending.
     */ 
    pixman_image_composite(
        PIXMAN_OP_SRC,           // PIXMAN_OP_SRC means result = source (with transformations applied); no blending happens.
        src_img, NULL, dest_img, // source, mask and destination
        0, 0,                    // source offset
        0, 0,                    // mask offset
        0, 0,                    // destination offset
        dest_width, dest_height
    );

    print_ascii(dest_buffer, dest_width, dest_height);

    save_buffer("/tmp/dest.binary", dest_buffer, dest_width * dest_height * 4);

    /*
     * Decrease reference count. On reaching 0, it free(3)s
     * - the pixman_image_t structure
     * - the bit buffer, if image->type == BITS && image->bits.free_me
     */
    pixman_image_unref(src_img);
    pixman_image_unref(dest_img);

    return 0;
}
