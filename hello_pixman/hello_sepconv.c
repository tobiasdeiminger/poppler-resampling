/*
  Compile:
  $ gcc `pkg-config --cflags pixman-1` hello_sepconv.c -Wl,`pkg-config --libs pixman-1` -lm
*/

#include <pixman.h> // the Pixman API
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/* ---- copied from cairo https://github.com/freedesktop/cairo/blob/7cb362d5861df09b990ea81bbbb66099cab3a2a5/src/cairo-image-source.c ---- */

#ifndef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif

#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif

/* Index into filter table */
typedef enum
{
    KERNEL_IMPULSE,
    KERNEL_BOX,
    /*KERNEL_LINEAR,
    KERNEL_MITCHELL,
    KERNEL_NOTCH,
    KERNEL_CATMULL_ROM,
    KERNEL_LANCZOS3,
    KERNEL_LANCZOS3_STRETCHED,
    KERNEL_TENT*/
} kernel_t;

/* Produce contribution of a filter of size r for pixel centered on x.
   For a typical low-pass function this evaluates the function at x/r.
   If the frequency is higher than 1/2, such as when r is less than 1,
   this may need to integrate several samples, see cubic for examples.
*/
typedef double (* kernel_func_t) (double x, double r);

/* Return maximum number of pixels that will be non-zero. Except for
   impluse this is the maximum of 2 and the width of the non-zero part
   of the filter rounded up to the next integer.
*/
typedef int (* kernel_width_func_t) (double r);

/* Table of filters */
typedef struct
{
    kernel_t kernel;
    kernel_func_t func;
    kernel_width_func_t width;
} filter_info_t;

/* PIXMAN_KERNEL_IMPULSE: Returns pixel nearest the center.  This
   matches PIXMAN_FILTER_NEAREST. This is useful if you wish to
   combine the result of nearest in one direction with another filter
   in the other.
*/
static double
impulse_kernel (double x, double r)
{
    return 1;
}

static int
impulse_width (double r)
{
    return 1;
}

/* PIXMAN_KERNEL_BOX: Intersection of a box of width r with square
   pixels. This is the smallest possible filter such that the output
   image contains an equal contribution from all the input
   pixels. Lots of software uses this. The function is a trapazoid of
   width r+1, not a box.
   When r == 1.0, PIXMAN_KERNEL_BOX, PIXMAN_KERNEL_LINEAR, and
   PIXMAN_KERNEL_TENT all produce the same filter, allowing
   them to be exchanged at this point.
*/

static double box_kernel (double x, double r)
{
    return MAX (0.0, MIN (MIN (r, 1.0), MIN ((r + 1) / 2 - x, (r + 1) / 2 + x)));
}

static int box_width (double r)
{
    return r < 1.0 ? 2 : ceil(r + 1);
}

static const filter_info_t filters[] =
{
    { KERNEL_IMPULSE, impulse_kernel, impulse_width },
    { KERNEL_BOX, box_kernel, box_width },
    /*{ KERNEL_LINEAR, linear_kernel, linear_width },
    { KERNEL_MITCHELL, mitchell_kernel, cubic_width },
    { KERNEL_NOTCH, notch_kernel, cubic_width },
    { KERNEL_CATMULL_ROM, cubic_kernel, cubic_width },
    { KERNEL_LANCZOS3, lanczos3_kernel, lanczos3_width },
    { KERNEL_LANCZOS3_STRETCHED,nice_kernel, nice_width },
    { KERNEL_TENT, tent_kernel, tent_width }*/
};

/* Fills in one dimension of the filter array */
static void get_filter(kernel_t filter, double r, int width, int subsample, pixman_fixed_t* out)
{
    int i;
    pixman_fixed_t *p = out;
    int n_phases = 1 << subsample;
    double step = 1.0 / n_phases;
    kernel_func_t func = filters[filter].func;

    /* special-case the impulse filter: */
    if (width <= 1) {
        for (i = 0; i < n_phases; ++i) {
            *p++ = pixman_fixed_1;
        }
        return;
    }

    for (i = 0; i < n_phases; ++i)
    {
        double frac = (i + .5) * step;
        /* Center of left-most pixel: */
        double x1 = ceil (frac - width / 2.0 - 0.5) - frac + 0.5;
        double total = 0;
        pixman_fixed_t new_total = 0;
        int j;

        for (j = 0; j < width; ++j)
        {
            double v = func(x1 + j, r);
            total += v;
            p[j] = pixman_double_to_fixed (v);
        }

        /* Normalize */
        total = 1 / total;
        for (j = 0; j < width; ++j)
            new_total += (p[j] *= total);

        /* Put any error on center pixel */
        p[width / 2] += (pixman_fixed_1 - new_total);
        p += width;
    }
}

/* Create the parameter list for a SEPARABLE_CONVOLUTION filter
 * with the given kernels and scale parameters.
 */
static pixman_fixed_t * create_separable_convolution (int *n_values, kernel_t xfilter, double sx, kernel_t yfilter, double sy)
{
    int xwidth, xsubsample, ywidth, ysubsample, size_x, size_y;
    pixman_fixed_t *params;

    xwidth = filters[xfilter].width(sx);
    xsubsample = 0;
    if (xwidth > 1) {
        while (sx * (1 << xsubsample) <= 128.0) xsubsample++;
    }
    size_x = (1 << xsubsample) * xwidth;

    ywidth = filters[yfilter].width(sy);
    ysubsample = 0;
    if (ywidth > 1) {
        while (sy * (1 << ysubsample) <= 128.0) ysubsample++;
    }
    size_y = (1 << ysubsample) * ywidth;

    *n_values = 4 + size_x + size_y;
    params = malloc (*n_values * sizeof (pixman_fixed_t));
    if (!params) return 0;

    params[0] = pixman_int_to_fixed (xwidth);
    params[1] = pixman_int_to_fixed (ywidth);
    params[2] = pixman_int_to_fixed (xsubsample);
    params[3] = pixman_int_to_fixed (ysubsample);

    get_filter(xfilter, sx, xwidth, xsubsample, params + 4);
    get_filter(yfilter, sy, ywidth, ysubsample, params + 4 + size_x);

    return params;
}

/* ---- end copy from cairo ---- */



/* Utility function to write buffer to file. One can view it in GIMP as "Raw image from data". */
static void save_buffer(char* filename, uint32_t *buf, uint32_t bufsize) {
    FILE* f;
    f = fopen(filename, "wb");
    fwrite(buf, 1, bufsize, f);
    fclose(f);
}

/* Utility function to print colors er pixel on terminal. */
static void print_ascii(uint32_t *buf, int width, int height) {
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            const uint32_t pix = buf[y * height + x];
            const uint32_t a = (pix & 0xff000000) >> 24;
            const uint32_t r = (pix & 0x00ff0000) >> 16;
            const uint32_t g = (pix & 0x0000ff00) >> 8;
            const uint32_t b = (pix & 0x000000ff);
            printf("%02x,%02x,%02x,%02x", a, r, g, b);
            if (x < width-1)
                printf(" ");
        }
        printf("\n");
    }
        
}

/* Utility function to draw a test image into a buffer. */
static void draw_test_image_a8r8g8b8(uint32_t *buffer, int width, int height) {
    for (int y = 0; y < height; y++)
        for (int x = 0; x < width; x++) {
            uint32_t pixel;
            if (y == 0 || y == height - 1 || x == 0 || x == width - 1)
                pixel = (0xff << 24) | (255 << 16);
            else
                pixel = (0xff << 24) | (255 * (y % 2));
            buffer[y * height + x] = pixel;
        }
}

int main (int argc, char **argv) {
    const float scale = 0.5;
    const int width = 32, height = 32;
    const int dest_width = width * scale, dest_height = height * scale;
    uint32_t *source_buffer, *dest_buffer;

    /*
     * pixman_image_t is pixmans handle for an image.
     * A image may be a bitmap, in which case a byte buffer containg the raw pixels is associated (image->type == BITS).
     */
    pixman_image_t *src_img, *dest_img;

    /* A fixed point type of size 4, where 16 MSB are the integer part and 16 LSB are fractional part. */
    pixman_fixed_t s;

    /* Prepare source buffer and image. Pixman won't allocate the buffer, we have to do it manually. */
    source_buffer = calloc (1, width * height * sizeof(uint32_t));
    draw_test_image_a8r8g8b8(source_buffer, width, height);
    src_img = pixman_image_create_bits(
        PIXMAN_a8r8g8b8, // A pixel shall have 4 bytes = 32 bits: 8 bits for alpha, 8 bits for red, 8 bits for green, 8 bits for blue.
        width, height,   // width and height in pixels
        source_buffer,   // the underlying byte buffer
        width * 4        // Rowstride bytes, for cases where one needs padding after each row,
                         // to align rows on memory boundaries. We don't require padding here,
                         // so rowstride is simply size of row.
    );

    /* Prepare destination buffer and image. */
    dest_buffer = calloc (1, dest_width * dest_height * sizeof(uint32_t)); // allocate and 0-initilaize
    dest_img = pixman_image_create_bits(
        PIXMAN_a8r8g8b8,
        dest_width, dest_height,
        dest_buffer,             
        dest_width * 4           
    );

   /*
    * A pixman_transform_t is simply a 3x3 matrix.
    * We can represents projective (much more often affine)
    * 2d transformations, i.e. describe scale, translation and rotation.
    *
    * MIND THE UNEXPECTED: It is used as a destination-to-source matrix.
    * So we have to give numbers like "source is double (i.e. sx=2) the size of destination",
    * instead of "destination is half (sx=0.5) the size of source".
    */
    pixman_transform_t transform;

    /*
     * Calculate reverse (dest-to-source) scale, and convert it to fixed point type.
     * 65536 is 0x00010000. Multiplying something with 65536.0 basically means shift left by 16 bits,
     * but take factional part into account instead of filling right bits with 0.
     */
    s = (1 / scale) * 65536.0 + 0.5;
    printf("Scale source by %f (dest-to-source sx=sy=%u.%u)\n", scale, (s & 0xffff0000) >> 16, (s & 0x0000ffff));

    /*
     * Set matrix parameters so that they describe a scaling operation with
     * sx = s and sy = s. If s = 2, this means "source is double the size of destination",
     * which we more intuitively read as "destination is half the size of source".
     */
    pixman_transform_init_scale (&transform, s, s);

    /*
     * Set the transformation matrix.
     * It is used when we use that image as a source in a composite call later.
     */
    pixman_image_set_transform (src_img, &transform); // associate source image with the scaling transformation

    /*
     * The result of the scale transformation is stored in a 2D raster.
     * Input raster and output raster have different sizes,
     * so we need a filter for resampling.
     * It is used when we use that image as a source in a composite call later.
     */
    /*pixman_image_set_filter(
        src_img,
        PIXMAN_FILTER_BILINEAR,
        NULL,
        0
    );*/

    /*
     * The SEPARABLE_CONVOLUTION API is about calculating a n x n convolution matrix yourself,
     * which needs some math not provided by pixman.
     */
    double dx = 1. / scale; // dest-to-source x scale
    double dy = 1. / scale; // dest-to-source y scale
    int n_params;
    pixman_fixed_t *params;
    /* Clip the filter size to prevent extreme slowness. This value could be raised if 2-pass filtering is done */
    if (dx > 16.0) dx = 16.0;
    if (dy > 16.0) dy = 16.0;
    /* Resulting convolution matrix will match the bilinear filter for scales > .75: */
    if (dx < 1.0/0.75) dx = 1.0;
    if (dy < 1.0/0.75) dy = 1.0;
    params = create_separable_convolution(
        &n_params,   // matrix to be filled
        KERNEL_BOX,  // kernel in x direction
        dx,          // scale parameter in x direction
        KERNEL_BOX,  // kernel in y direction
        dy           // scale parameter in y direction
    );
    
    printf("Convolution matrix with %i filter parameter.\n", n_params);
    pixman_image_set_filter(src_img, PIXMAN_FILTER_SEPARABLE_CONVOLUTION, params, n_params);

    /*
     * Despite the name, pixman_image_composite() seems to be the
     * general purpose "now do it" API function in pixman.
     * (how about renaming it pixman_image_operate()?).
     *
     * It can not only blend two images, but it's also the way to go to
     * apply transformations and filters to the source image.
     * Whatever the operation is, the result will be stored into
     * the destination buffer.
     *
     * The first argument PIXMAN_OP_* refers to Porter Duff operators.
     * Pixman contributor Søren Sandmann Pedersen explains them well:
     * http://ssp.impulsetrain.com/porterduff.html
     *
     * Note that pixman always assumes premultiplied alpha when blending.
     */ 
    pixman_image_composite(
        PIXMAN_OP_SRC,           // PIXMAN_OP_SRC means result = source (with transformations applied); no blending happens.
        src_img, NULL, dest_img, // source, mask and destination
        0, 0,                    // source offset
        0, 0,                    // mask offset
        0, 0,                    // destination offset
        dest_width, dest_height
    );

    print_ascii(dest_buffer, dest_width, dest_height);

    save_buffer("/tmp/dest.binary", dest_buffer, dest_width * dest_height * 4);

    /*
     * Decrease reference count. On reaching 0, it free(3)s
     * - the pixman_image_t structure
     * - the bit buffer, if image->type == BITS && image->bits.free_me
     */
    pixman_image_unref(src_img);
    pixman_image_unref(dest_img);

    return 0;
}
